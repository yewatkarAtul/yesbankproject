package StepDefinition;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import POM.BlockListAccountModule;
import POM.LogIn_YesBank;
import io.cucumber.java.en.*;

public class Setp_For_Block_List_Account 
{
  
	   public WebDriver driver ;
	   LogIn_YesBank Lg ;
	   BlockListAccountModule ba ; 
	   
      @Given ("User Launch Chrome browser for YesBank")
      public void User_Launch_Chrome_browser_yesBank () 
      {
    	  System.setProperty("webdriver.chrome.driver",
	                 "driver/chromedriver.exe");
                    driver = new ChromeDriver () ;
   	    Lg = new LogIn_YesBank (driver);
   	    ba = new BlockListAccountModule (driver) ;
   	    
      }
  	
  	@When ("User opens URL of Yesbank {string}")
      public void user_opens_URL(String url)
      {  
   
      	driver.manage().window().maximize();
   	    driver.get(url);
      }
  	

      @And ("User enters EmailAddress and Password")
     public void user_enters_Email_and_Password() throws InterruptedException
     { 
      	Thread.sleep(2000);
      	Lg.ClickOnAdvance();
      	Thread.sleep(2000);
      	Lg.ClickOnProceedUnsafe();
      	Thread.sleep(2000);
      	Lg.EnterUserId();
      	Thread.sleep(2000);
      	Lg.EnterPassword();
      	Thread.sleep(2000);
      	Lg.clickOnLogIn();
     }
    
      
      @When ("Add The BlockListAccount SuccessFully") 
      public void Add_The_BlockListAccount_SuccessFully () throws InterruptedException 
      {   
    	   Thread.sleep(2000);
    	   driver.switchTo().frame("toc");
    	   ba.ClickOnApplication();
    	   Thread.sleep(2000);
    	   ba.ClickOnMasterDataMaintenance();
    	   Thread.sleep(2000);
    	   ba.ClickOnBlockListMaster();
    	   Thread.sleep(2000);
    	   ba.ClickOnBlockListAccount();
    	   Thread.sleep(2000);
    	   ba.ClickOnEnter();
    	   Thread.sleep(2000);
    	   driver.switchTo().defaultContent();
    	   
    	ArrayList<String> al= new   ArrayList<String> (driver.getWindowHandles()) ;
    	
    	driver.switchTo().window(al.get(1)) ;
    	
    	   ba.enterAccountNumber();
    	   Thread.sleep(2000);
    	   ba.ClickOnOk();
    	   Thread.sleep(2000);
    	   ba.ClickSubmitAccount();
    	   driver.close();
    	driver.switchTo().window(al.get(0))   ;
    	
      }
      
      
      @And("Click on LogOut of_YesBank")
      public void Click_on_LogOut_Yesbank() throws InterruptedException 
      { 
      	Thread.sleep(3000);
      	driver.switchTo().frame("app");
      	Thread.sleep(2000);
      	Lg.clickOnLogOutButton();
      	driver.switchTo().parentFrame();
          Thread.sleep(2000);
          driver.switchTo().frame("content");
          Lg.clickOnOkbutton();
          Thread.sleep(2000);
          Lg.clickOnOkAcceptbutton();
    
      }
          
      
      @ And ("Close the Browser of YesBank")
      public void CloseBrowser() throws InterruptedException 
      {
      	 Thread.sleep(2000);
      	 driver.quit();
      }
     
}
