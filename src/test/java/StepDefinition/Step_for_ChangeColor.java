package StepDefinition;

import org.openqa.selenium.WebDriver;

import POM.Change_Header_Colors;
import io.cucumber.java.en.*;


public class Step_for_ChangeColor 
{
	   public WebDriver driver ;
	    
	    Change_Header_Colors  change ;
	    
	    
	   @And  ("Change Header Color")
	    public void Change_Header_Color_of_Application() throws InterruptedException
	    {  
		    change = new Change_Header_Colors(driver);
		   
	    	driver.switchTo().frame("app");
			Thread.sleep(2000);
			change.ClickOnSetting();
			Thread.sleep(2000);
		    driver.switchTo().parentFrame();
			Thread.sleep(2000);
		    driver.switchTo().frame("content");
			change.ClickOnColor();
			Thread.sleep(2000);
			change.ClickOnSave();
			driver.switchTo().defaultContent();
			
	    }
		
}
