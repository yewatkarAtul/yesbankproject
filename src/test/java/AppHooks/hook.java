package AppHooks;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;



public class hook 
{

	WebDriver driver ;
	
	@Before
   public void stepUp () 
   {

  	    System.setProperty("webdriver.chrome.driver",
  	    		                 "driver/chromedriver.exe");
  	    driver = new ChromeDriver () ;
   }
   
   @After
   public void tearDown () throws InterruptedException 
   {
	     driver.quit();
	     Thread.sleep(1000);
   }
	
}
