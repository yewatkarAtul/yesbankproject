package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogIn_YesBank 
{
   
	public WebDriver driver;
	
	@FindBy(xpath="//button[@id='details-button']" )
	    WebElement AdvanceOption ;
	
	@FindBy(xpath="//a[@id='proceed-link']" )
	    WebElement ProceedUnsafe ;
	
	@FindBy(xpath="//input[@name='j_user']" )
        WebElement UserId ;

	@FindBy(xpath="(//input[@type='password'])[2]")
        WebElement passwords ;

	@FindBy(xpath="//button[@id='login']")
        WebElement ClickLogIn ;
	
	@FindBy(xpath = "//*[@id=\"logoutButtonIcon\"]/span")
	    WebElement ClickLogOut ;
	
	@FindBy(xpath="(//button[text()='OK'])[2]")
	    WebElement ClickOnOk ;
	
	@FindBy(xpath="//a[text()='Accept']")
        WebElement ClickOnAccept ;	    
	                                                       
	public LogIn_YesBank(WebDriver driver) 
	{
		PageFactory.initElements(driver, this);
	}   
	                   
	
	public void ClickOnAdvance() 
	{
		AdvanceOption.click();
	}
	
	public void ClickOnProceedUnsafe() 
	{
		ProceedUnsafe.click();
	}
	
	public void EnterUserId() 
	{
		UserId.sendKeys("Atul_Test");
	}

	public void EnterPassword() 
	{
		passwords.sendKeys("Atul@9673");
	}

	public void clickOnLogIn() 
	{
		ClickLogIn.click();
	}
	
	public void clickOnLogOutButton() 
	{
		
		ClickLogOut.click();
	}
	
	public void clickOnOkbutton() 
	{
		ClickOnOk.click();
	}
	public void clickOnOkAcceptbutton() 
	{
		ClickOnAccept.click();
	}

}
