package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Change_Header_Colors
{
       public  WebDriver driver ;
   
      @FindBy(xpath="//span[@class='glyphicon glyphicon-cog']" )
          WebElement Setting ;
    
      @FindBy (xpath="//button[text()='Orange   ']" )
          WebElement colors ;
   
 
      @FindBy (xpath="//input[@id='themeSetup']")
          WebElement SaveColor ;
   
     public Change_Header_Colors (WebDriver driver) 
     {
    	 PageFactory.initElements( driver , this );
     }
   
      public void ClickOnSetting () 
       {
    	   Setting.click();
       } 
      
      public void ClickOnColor () 
       {
    	    SaveColor.click();
       }
      
      public void ClickOnSave () 
      {
   	     colors.click();
      }
}
