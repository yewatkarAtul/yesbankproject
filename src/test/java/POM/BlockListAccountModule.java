package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BlockListAccountModule 
{
        public WebDriver driver ;
        
        @FindBy(xpath="//*[@id=\"1l3\"]/a")
            WebElement application ;
        
        @FindBy (xpath="//*[@id=\"1l3l6\"]/a")
            WebElement MasterDataMaintenance  ;
      
        @FindBy (xpath="//*[@id=\"1l3l6l8\"]/a")
            WebElement  BlockListMaster  ;
        
        @FindBy (xpath="//*[@id=\"1l3l6l8l1\"]/a")
            WebElement  BlockListAccount ;
        
        @FindBy (xpath="//*[@id=\"1l3l6l8l1l2\"]/a")
            WebElement  Enter; 
        
        @FindBy (xpath="//input[@name='accountNum']")
        WebElement  accountNumber;
        
        @FindBy (xpath="//*[@id=\"pageBody\"]/table/tbody/tr/td/table/tbody/tr[5]/td/input[3]")
        WebElement  okAccount;
        
        @FindBy (xpath="//*[@id=\"pageBody\"]/div[2]/form[1]/input")
        WebElement  submitAccount;
        
        public BlockListAccountModule (WebDriver driver) 
          {
        	  PageFactory.initElements( driver , this);
          }
        
        
        public void ClickOnApplication() 
    	{
        	application.click();
    	} 
        

        public void ClickOnMasterDataMaintenance() 
    	{
        	MasterDataMaintenance.click();
    	} 

        public void ClickOnBlockListMaster () 
    	{
        	BlockListMaster.click();
    	} 
        

        public void  ClickOnBlockListAccount() 
    	{
        	BlockListAccount.click();
    	} 
        
        public void  ClickOnEnter() 
    	{
        	Enter.click();
    	} 
        
        public void  enterAccountNumber() 
    	{
        	accountNumber.sendKeys("90217822181994");
    	}
        
        public void  ClickOnOk() 
    	{
        	okAccount.click();
    	}

        public void  ClickSubmitAccount() 
    	{
        	submitAccount.click();
    	}
}
