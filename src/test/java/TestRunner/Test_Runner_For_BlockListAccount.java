package TestRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith (Cucumber.class)

@CucumberOptions 
(
		  features = {"src/test/java/Feature/blockListAccount.feature"},
		  glue = {"AppHooks","StepDefinition"},
		  dryRun = false ,
		  monochrome=true,
		  plugin = {"pretty", "html:report1"}
		  
)


public class Test_Runner_For_BlockListAccount {

}
